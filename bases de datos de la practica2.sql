﻿/* creando las bases de datos */

-- creamos base de datos para meter las tablas del script original
DROP DATABASE IF EXISTS practica2;
CREATE DATABASE IF NOT EXISTS practica2;

-- seleccionamos la base de datos recien creada
USE practica2;

-- script original
CREATE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población Integer
);

CREATE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30)
 );

CREATE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30)
);

CREATE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario Integer
);

CREATE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona)
);

