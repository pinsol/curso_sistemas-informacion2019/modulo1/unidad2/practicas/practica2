﻿                                                                  /** MODULO I - UNIDAD II - PRACTICA 2 **/

USE practica2;

/* 1. Indicar el número de ciudades que hay en la tabla ciudades */
SELECT
  COUNT(*) nCiudades
FROM
  ciudad c
;

/* 2. Indicar el nombre de las ciudades que tengan una población por encima de la población media */
CREATE OR REPLACE VIEW mediaPoblacion AS 
SELECT AVG(c.población) media FROM ciudad c
;

SELECT
  c.nombre
FROM
  ciudad c
WHERE
  c.población > (SELECT media FROM mediapoblacion)
;

/* 3. Indicar el nombre de las ciudades que tengan una población por debajo de la población media */
SELECT
  c.nombre
FROM
  ciudad c
WHERE
  c.población < (SELECT media FROM mediapoblacion)
;

/* 4. Indicar el nombre de la ciudad con la población máxima */
SELECT
  c.nombre
FROM
  ciudad c
WHERE
  c.población = (SELECT MAX(c1.población) FROM ciudad c1)
;

/* 5. Indicar el nombre de la ciudad con la población mínima */
SELECT
  c.nombre
FROM
  ciudad c
WHERE
  c.población = (SELECT MIN(c1.población) FROM ciudad c1)
;

/* 6. Indicar el número de ciudades que tengan una población por encima de la población media */
SELECT
  COUNT(*) nCiudadesSuperiorMedia
FROM
  ciudad c
WHERE
  c.población > (SELECT media FROM mediapoblacion)
;

/* 7. Indicarme el número de personas que viven en cada ciudad */
SELECT
  p.ciudad,
  COUNT(*) nPersonas
FROM
  persona p
    GROUP BY p.ciudad
;

/* 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias */
CREATE OR REPLACE VIEW nTrabajadoresXcia AS
SELECT
  t.compañia,
  COUNT(*) nPersonas
FROM
  trabaja t
    GROUP BY t.compañia
;

SELECT
  * 
FROM
  nTrabajadoresXcia tx
;

/* 9. Indicarme la compañía que mas trabajadores tiene */
SELECT
  compañia
FROM
  nTrabajadoresXcia tx
WHERE
  nPersonas =
  (SELECT MAX(nPersonas) FROM nTrabajadoresXcia tx)
;

/* 10. Indicarme el salario medio de cada una de las compañias */
SELECT
  t.compañia,
  AVG(t.salario) mediaSalario
FROM
  trabaja t
    GROUP BY t.compañia
;

/* 11. Listarme el nombre de las personas y la población de la ciudad donde viven */
SELECT
  p.nombre,
  c.población
FROM
  persona p
  JOIN ciudad c ON p.ciudad = c.nombre
;

/* 12. Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive */
SELECT
  p.nombre,
  p.calle,
  c.población
FROM
  persona p
  JOIN ciudad c ON p.ciudad = c.nombre
;

/* 13. Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja */
SELECT
  p.nombre,
  p.ciudad,
  c.ciudad
FROM
  persona p
  JOIN trabaja t ON t.persona = p.nombre
  JOIN compañia c ON t.compañia = c.nombre
;

/* 14. Explicar la siguiente consulta

SELECT Persona.nombre                     -- Muestra las personas
FROM Persona, Trabaja, Compañia           -- realiza un 'JOIN' o uniones de tablas a partir de WHERE
WHERE Persona.nombre = Trabaja.persona    \
AND Trabaja.compañia = Compañia.nombre     -> establece los campos de union de las tablas
AND Compañia.ciudad = Persona.ciudad      /
ORDER BY Persona.nombre;                  -- ordena por nombre de persona el resultado

*/

  -- RESPUESTA: Muestra en orden alfabético las personas que trabajan en la misma ciudad en la que vive.
                -- Se podría implementar mejor con un JOIN.

/* 15. Listarme el nombre de la persona y el nombre de su supervisor */
SELECT
  s.persona,
  s.supervisor
FROM
  supervisa s
;

/*
  16. Listarme el nombre de la persona, el nombre de su supervisor
  y las ciudades donde residen cada una de ellos
*/
SELECT
  p.nombre persona,
  s.supervisor,
  p.ciudad ciudadPersona,
  p1.ciudad ciudadSupervisor
FROM
  supervisa s
  JOIN persona p ON p.nombre = s.persona
  JOIN persona p1 ON s.supervisor = p1.nombre
;

/* 17. Indicarme el número de ciudades distintas que hay en la tabla compañía */
SELECT
  COUNT(DISTINCT c.ciudad) nCiudadesDistintas
FROM
  compañia c
;

/* 18. Indicarme el número de ciudades distintas que hay en la tabla personas */
SELECT
  COUNT(DISTINCT p.ciudad) nCiudadesDistintas
FROM
  persona p
;

/* 19. Indicarme el nombre de las personas que trabajan para FAGOR */
SELECT
  t.persona
FROM
  trabaja t
WHERE
  t.compañia = 'FAGOR'
;

/* 20. Indicarme el nombre de las personas que no trabajan para FAGOR */
SELECT
  t.persona
FROM
  trabaja t
WHERE
  t.compañia <> 'FAGOR'
;

/* 21. Indicarme el número de personas que trabajan para INDRA */
SELECT
  COUNT(*) nTrabajadoresINDRA
FROM
  trabaja t
WHERE
  t.compañia = 'INDRA'
;

/* 22. Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA */
SELECT
  t.persona
FROM
  trabaja t
WHERE
  t.compañia = 'FAGOR'
  OR
  t.compañia = 'INDRA'
;

/*
  23. Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. Ordenar la
      salida por nombre de la persona y por salario de forma descendente
*/
SELECT
  c.población,
  t.salario,
  p.nombre,
  t.compañia
FROM
  persona p
  JOIN trabaja t ON p.nombre = t.persona
  JOIN ciudad c ON p.ciudad = c.nombre
;